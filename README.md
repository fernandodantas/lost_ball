## Lost Ball

Lost Ball is a maze game where the player will use the movement of his cell to 
guide a ball out of a maze. Each game has a countdown timer, and the level of 
difficulty increases as the player advances through the game.

Genre: Puzzle